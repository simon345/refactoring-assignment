Simon Duffy C12379556
Bitbucket Repo - https://bitbucket.org/simon345/refactoring-assignment

My overall goal for this was to try separate the GUI from the model in a sort of MVC pattern.

First thing I did was just a general clean up. I renamed variables and created separate ActionListener classes for any buttons with a reasonable amount of functionality or Buttons that appeared more than once (e.g. creating ReturnButtonListener for the ReturnButton that appears in several classes such as ExistingCustomerAccountSelectionPanel.java)

public class ReturnButtonListener implements ActionListener {
	
	private Menu menu;
	
	public ReturnButtonListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		menu.getFrame().dispose();
		menu.mainMenu();
	}

}


I cleaned up the entities (the classes in com.bank.entities). I appended private to all the properties and gave them all getters and setters. I also rearranged all classes into relevant packages. In Customer.java I created helper methods i.e. hasNoAccounts() to return if the customer has any accounts in the Account List. And getAccountByNumber() which returns a CustomerAccount object.

	public CustomerAccount getAccountByNumber(String selectedItem) {
		
		CustomerAccount account = null;
		
		for (CustomerAccount acc : accounts) {
			if (acc.getNumber() == selectedItem) {
				account = acc;
				break;
			}
		}
		
		return account;
		
	}

	public boolean hasNoAccounts() {
		
		if(accounts == null) {
			accounts = new ArrayList<CustomerAccount>();
		}
		
		return accounts.isEmpty();
	}

I created a BankAccountFactory that was in charge of creating either a Deposit or Current account. This is called in AccountButtonListener.java. A customer is returned with the newly created account.

public Customer addAccountToCustomer(String accountTypeChoice, Customer customer, List<Customer> customerList) {

			customer.addAccount(createNewCurrentAccount());
or
			customer.addAccount(createNewDepositAccount());

I created an Enum called TransactionType which could be either a Lodgement or a Withdrawal. This was to replace comparing Strings with “lodgement” or “Withdrawal”. Using an enum is better practice for this. Also in AccountTransaction I replaced the String date with a Date object.

	LODGEMENT("Lodgement"), WITHDRAWAL("Withdrawal");

public class AccountTransaction {

	private Date date;
	private double amount;
	private TransactionType type;

There was a specific JFrame that was repeatedly created that, for example, has a size of 400 by 400. So I created a class called DefaultJFrame with these common attributes. This was done to reduce repeating code. 

private static final long serialVersionUID = 3400032004930333853L;
	private static final int WIDTH = 400;
	private static final int HEIGHT = 400;
	private static final int X_LOCATION = 200;
	private static final int Y_LOCATION = 200;

	public DefaultJFrame(String title) {
		
		this.setTitle(title);
		this.setSize(WIDTH, HEIGHT);
		this.setLocation(X_LOCATION, Y_LOCATION);
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		
		
	}

I made custom panels. This wasn’t necessarily to reduce code use, it was more for readability sake. AdminstratorMenuPanel.java was a panel with several buttons and a label. It is created in Menu.java in the adminstrationMenu() method. Making the code clearer was the reason for creating the other 4 JPanel classes too.

I created an entity called Bank.java which is a property of Menu.java. I did this to separate the data from the GUI. It makes it so Menu.java just has one object called Bank instead of a list of Customer objects, a Customer object and a CustomerAccount object. This also allowed Bank.java to have helper methods such as addCustomer(Customer customer). This helped maintain Menu.java SRP.

public class Bank {

	private ArrayList<Customer> customers;
	private Customer customer;
	private CustomerAccount acc;

	public void addCustomer(Customer customer) {
		
		if(customers == null) {
			customers = new ArrayList<Customer>();
		}
		
		customers.add(customer);
	}	

I created TransactionOperations is the logic to either make a lodgement or make a withdrawal . It is also used to apply bank charges. The reason for this account was to again separate the logic from the GUI. I also made it because the code in LodgementButtonListener and WithdrawalButtonListener was essentially a direct copy, the only difference is a lodgement increases the balance and withdrawal decreases. Creating this class significantly reduced the amount of code in LodgementButonListener and WithdrawalButtonListener.

I refactored NavigateButtonListener which is used to display the customer by creating displayCustomer(Customer customer). In each button listener I call this method with the correct customer. This greatly reduced the amount of repeated code which would help if I needed to add another customer property for example.

		first.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					Customer firstCustomer = customerList.get(0);
					displayCustomer(firstCustomer);
				}
			});

			previous.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {

					if (position >= 1) {
						position = position - 1;
						Customer currentCustomer = customerList.get(position);
						displayCustomer(currentCustomer);

					}
				}
			});

	private void displayCustomer(Customer customer) {

		firstNameTextField.setText(customer.getFirstName());
		surnameTextField.setText(customer.getSurname());
		pPSTextField.setText(customer.getPPS());
		dOBTextField.setText(customer.getDOB());
		customerIDTextField.setText(customer.getId());
		passwordTextField.setText(customer.getPassword());
	}

There is also some JUnit testing