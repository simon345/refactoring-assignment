package com.bank.main;

import com.bank.entities.Bank;

public class Main {

	public static void main(String[] args) {

		Bank bank = new Bank();
		Menu driver = new Menu();
		driver.setBank(bank);
		driver.mainMenu();
	}

}
