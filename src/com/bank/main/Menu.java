package com.bank.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.bank.entities.Bank;
import com.bank.entities.Customer;
import com.bank.jframes.DefaultJFrame;
import com.bank.jpanels.AdminstratorMenuPanel;
import com.bank.jpanels.ExistingCustomerAccountSelectionPanel;
import com.bank.jpanels.MainMenuPanel;
import com.bank.listeners.MainMenuButtonListener;
import com.bank.listeners.ReturnButtonListener;

public class Menu extends JFrame {


	/**
	 * 
	 */
	private static final long serialVersionUID = -3310963870412014868L;

	private JFrame frame;
	private Container content;
	private Bank bank;

	public void mainMenu() {

		if(frame != null) {
			frame.dispose();
		}
		
		frame = new DefaultJFrame("Main Menu");
		setFrame(frame);
		
		MainMenuPanel mainMenu = new MainMenuPanel();
		JPanel buttonPanel = new JPanel();
		
		JButton continueButton = new JButton("Continue");
		buttonPanel.add(continueButton);

		Container content = frame.getContentPane();
		content.setLayout(new GridLayout(2, 1));
		content.add(mainMenu);
		content.add(buttonPanel);

		MainMenuButtonListener continueButtonListener = new MainMenuButtonListener(this, mainMenu.getUserType(), frame, bank);
		continueButton.addActionListener(continueButtonListener);

		frame.setVisible(true);

		
	}

	public void adminstrationMenu() {
		
		System.out.println("admin()");
		
		if(frame != null) {
			frame.dispose();
		}

		frame = new DefaultJFrame("Administrator");
		frame.setVisible(true);
		setFrame(frame);
		
		AdminstratorMenuPanel adminPanel = new AdminstratorMenuPanel(this);
		
		JPanel returnPanel = new JPanel();
		JButton returnButton = new JButton("Exit Admin Menu");
		returnPanel.add(returnButton);

		ReturnButtonListener returnButtonActionListener = new ReturnButtonListener(this);
		returnButton.addActionListener(returnButtonActionListener);

		content = frame.getContentPane();
		content.setLayout(new BorderLayout());
		content.add(adminPanel, BorderLayout.NORTH);
		content.add(returnPanel, BorderLayout.SOUTH);

	}

	public void existingCustomerMenu(Customer currentCustomer) {
		
		System.out.println("customer()");
		
		if(frame != null) {
			frame.dispose();
		}
		
		frame = new DefaultJFrame("Customer Menu");
		frame.setVisible(true);
		setFrame(frame);
		
		bank.setCustomer(currentCustomer);

		if (currentCustomer.hasNoAccounts()) {
			
			JOptionPane.showMessageDialog(frame, "This customer does not have any accounts yet. \n An admin must create an account for this customer \n for them to be able to use customer functionality. ", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			frame.dispose();
			mainMenu();
			
		} else {
			
			ExistingCustomerAccountSelectionPanel existingCustomerPanel = new ExistingCustomerAccountSelectionPanel(this);
			content = frame.getContentPane();
			content.add(existingCustomerPanel);

		}
	}

	public boolean isNumeric(String str) {
		try {
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public Container getContent() {
		return content;
	}

	public void setContent(Container content) {
		this.content = content;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}
}
