package com.bank.enums;

public enum TransactionType {
	
	LODGEMENT("Lodgement"), WITHDRAWAL("Withdrawal");
	
	@SuppressWarnings("unused")
	private final String name;
	
	private TransactionType(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name();
	}

}
