package com.bank.jframes;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class DefaultJFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3400032004930333853L;
	private static final int WIDTH = 400;
	private static final int HEIGHT = 400;
	private static final int X_LOCATION = 200;
	private static final int Y_LOCATION = 200;

	public DefaultJFrame(String title) {
		
		this.setTitle(title);
		this.setSize(WIDTH, HEIGHT);
		this.setLocation(X_LOCATION, Y_LOCATION);
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		
		
	}
	
}
