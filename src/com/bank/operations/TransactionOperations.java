package com.bank.operations;

import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.bank.entities.AccountTransaction;
import com.bank.entities.Bank;
import com.bank.entities.Customer;
import com.bank.entities.CustomerAccount;
import com.bank.entities.CustomerCurrentAccount;
import com.bank.entities.CustomerDepositAccount;
import com.bank.enums.TransactionType;
import com.bank.main.Menu;

public class TransactionOperations {
	
	private TransactionType type;
	private CustomerAccount account;
	private Menu menu;
	private Bank bank;
	private Customer customer;
	private JFrame frame;
	private String euro = "\u20ac";

	public TransactionOperations(TransactionType type, CustomerAccount account, Menu menu) {

		this.type = type;
		this.account = account;
		this.menu = menu;
		this.bank = menu.getBank();
		this.customer = bank.getCustomer();
		this.frame = menu.getFrame();
	}
	
	public TransactionOperations(CustomerAccount account, JFrame frame) {
		this.account = account;
		this.frame = frame;
	}
	
	public TransactionOperations(CustomerAccount account, JFrame frame, Menu menu) {
		this.frame = frame;
		this.menu = menu;
		this.account = account;
	}

	public CustomerAccount addInterestToAccount() {
		
		double interest = 0;
		boolean interestStringIsNumeric = false;

		while (!interestStringIsNumeric) {
			String interestString = JOptionPane.showInputDialog(frame, "Enter interest percentage you wish to apply: \n NOTE: Please enter a numerical value. (with no percentage sign) \n E.g: If you wish to apply 8% interest, enter '8'");

			if (menu.isNumeric(interestString)) {

				interest = Double.parseDouble(interestString);
				interestStringIsNumeric = true;

				double currentBalance = account.getBalance();
				double newBalance = calculateInterest(interest, currentBalance);
				account.setBalance(newBalance);

				JOptionPane.showMessageDialog(frame, interest + "% interest applied. \n new balance = " + account.getBalance() + euro, "Success!", JOptionPane.INFORMATION_MESSAGE);
			}

			else {
				JOptionPane.showMessageDialog(frame, "You must enter a numerical value!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			}

		}
		
		return account;
	}
	
	private double calculateInterest(double interest, double currentBalance) {
		return currentBalance + (currentBalance * (interest / 100));
	}
	
	public CustomerAccount applyBankCharge() {
		
		if (account instanceof CustomerDepositAccount) {

			JOptionPane.showMessageDialog(frame, "25" + euro + " deposit account fee aplied.", "", JOptionPane.INFORMATION_MESSAGE);
			account.setBalance(account.getBalance() - 25);
		}

		if (account instanceof CustomerCurrentAccount) {

			JOptionPane.showMessageDialog(frame, "15" + euro + " current account fee aplied.", "", JOptionPane.INFORMATION_MESSAGE);
			account.setBalance(account.getBalance() - 15);
		}
		
		JOptionPane.showMessageDialog(frame, "New balance = " + account.getBalance(), "Success!", JOptionPane.INFORMATION_MESSAGE);

		
		return account;
	}
	
	public CustomerAccount performTransaction() {
				
		if(account instanceof CustomerCurrentAccount) {
			
			System.out.println("CUSTOMER CURRENT ACCOUNT");
			
			int count = 3;
			int checkPin = ((CustomerCurrentAccount) account).getAtm().getPin();
			boolean isPinValid = true;
			boolean pinAttemptsRemaining = true;

			while (isPinValid) {
				
				if (count == 0) {
					JOptionPane.showMessageDialog(frame, "Pin entered incorrectly 3 times. ATM card locked.", "Pin", JOptionPane.INFORMATION_MESSAGE);
					((CustomerCurrentAccount) account).getAtm().setValid(false);
					menu.existingCustomerMenu(customer);
					isPinValid = false;
					pinAttemptsRemaining = false;
				}

				String pinEntered = JOptionPane.showInputDialog(frame, "Enter 4 digit PIN;");
				
				int pin = Integer.parseInt(pinEntered);

				if (pinAttemptsRemaining) {
					if (checkPin == pin) {
						isPinValid = false;
						JOptionPane.showMessageDialog(frame, "Pin entry successful", "Pin", JOptionPane.INFORMATION_MESSAGE);

					} else {
						count--;
						JOptionPane.showMessageDialog(frame, "Incorrect pin. " + count + " attempts remaining.", "Pin", JOptionPane.INFORMATION_MESSAGE);
					}

				}
			}
		}
		
		String userInputAmount = JOptionPane.showInputDialog(frame, type.toString() + ": Enter amount:");
		double amount = 0;
		
		if (menu.isNumeric(userInputAmount)) {

			amount = Double.parseDouble(userInputAmount);
			
			if(type.equals(TransactionType.LODGEMENT)) {
				createLodgementTransaction(amount);
			} else {
				createWithdrawalTransaction(amount);
			}
		
		} else {
			JOptionPane.showMessageDialog(frame, "You must enter a numerical value!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
		}


		return account;
		
	
	}
	
	private void createLodgementTransaction(double amount) {
		
		Date date = new Date();
		AccountTransaction transaction = new AccountTransaction(date, TransactionType.LODGEMENT, amount);
		double newBalance = account.getBalance() + amount;
		account.setBalance(newBalance);
		account.addTransaction(transaction);
		
		JOptionPane.showMessageDialog(frame, amount + euro + " added to your account!", "Lodgement", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(frame, "New balance = " + account.getBalance() + euro, "Lodgement", JOptionPane.INFORMATION_MESSAGE);

		
	}
	
	private void createWithdrawalTransaction(double withdraw) {
			
		if (withdraw > 500) {
			JOptionPane.showMessageDialog(frame, "500 is the maximum you can withdraw at a time.", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			withdraw = 0;
		} else if (withdraw > account.getBalance()) {
			JOptionPane.showMessageDialog(frame, "Insufficient funds.", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			withdraw = 0;
		} else {

			double newBalance = account.getBalance() - withdraw;
			account.setBalance(newBalance);
			
			Date date = new Date();
			double amount = withdraw;

			AccountTransaction transaction = new AccountTransaction(date, TransactionType.WITHDRAWAL, amount);
			account.addTransaction(transaction);

			JOptionPane.showMessageDialog(frame, withdraw + euro + " withdrawn.", "Withdraw", JOptionPane.INFORMATION_MESSAGE);
			JOptionPane.showMessageDialog(frame, "New balance = " + account.getBalance() + euro, "Withdraw", JOptionPane.INFORMATION_MESSAGE);

		}

	}
}
