package com.bank.listeners;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.bank.entities.Customer;
import com.bank.jframes.DefaultJFrame;
import com.bank.main.Menu;

public class EditCustomerButtonListener implements ActionListener {

	private Menu menu;
	private JFrame frame;
	private Customer customer;

	public EditCustomerButtonListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		frame = menu.getFrame();
		List<Customer> customerList = menu.getBank().getCustomers();
		customer = menu.getBank().getCustomer();
		boolean loop = true;

		boolean found = false;

		if (customerList.isEmpty()) {
			JOptionPane.showMessageDialog(frame, "There are no customers yet!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			frame.dispose();
			menu.adminstrationMenu();

		} else {

			while (loop) {
				String customerID = JOptionPane.showInputDialog(frame, "Enter Customer ID:");

				for (Customer aCustomer : customerList) {

					if (aCustomer.getId().equals(customerID)) {
						found = true;
						customer = aCustomer;
					}
				}

				if (found == false) {
					int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION) {
						loop = true;
					} else if (reply == JOptionPane.NO_OPTION) {
						frame.dispose();
						loop = false;

						menu.adminstrationMenu();
					}
				} else {
					loop = false;
				}

			}

			frame.dispose();
			frame = new DefaultJFrame("Edit Customer");
			frame.setVisible(true);
			frame.setSize(340, 350);
			frame.setLocation(200, 200);
			frame.setResizable(false);

			JLabel firstNameLabel = new JLabel("First Name:", SwingConstants.LEFT);
			JLabel surnameLabel = new JLabel("Surname:", SwingConstants.LEFT);
			JLabel ppsLabel = new JLabel("PPS Number:", SwingConstants.LEFT);
			JLabel dOBLabel = new JLabel("Date of birth", SwingConstants.LEFT);
			JLabel customerIDLabel = new JLabel("CustomerID:", SwingConstants.LEFT);
			JLabel passwordLabel = new JLabel("Password:", SwingConstants.LEFT);
			JTextField firstNameTextField = new JTextField(20);
			JTextField surnameTextField = new JTextField(20);
			JTextField pPSTextField = new JTextField(20);
			JTextField dOBTextField = new JTextField(20);
			JTextField customerIDTextField = new JTextField(20);
			JTextField passwordTextField = new JTextField(20);

			JPanel textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

			JPanel cancelPanel = new JPanel();

			textPanel.add(firstNameLabel);
			textPanel.add(firstNameTextField);
			textPanel.add(surnameLabel);
			textPanel.add(surnameTextField);
			textPanel.add(ppsLabel);
			textPanel.add(pPSTextField);
			textPanel.add(dOBLabel);
			textPanel.add(dOBTextField);
			textPanel.add(customerIDLabel);
			textPanel.add(customerIDTextField);
			textPanel.add(passwordLabel);
			textPanel.add(passwordTextField);

			firstNameTextField.setText(customer.getFirstName());
			surnameTextField.setText(customer.getSurname());
			pPSTextField.setText(customer.getPPS());
			dOBTextField.setText(customer.getDOB());
			customerIDTextField.setText(customer.getId());
			passwordTextField.setText(customer.getPassword());

			JButton saveButton = new JButton("Save");
			JButton cancelButton = new JButton("Exit");

			cancelPanel.add(cancelButton, BorderLayout.SOUTH);
			cancelPanel.add(saveButton, BorderLayout.SOUTH);

			Container content = frame.getContentPane();
			content.setLayout(new GridLayout(2, 1));
			content.add(textPanel, BorderLayout.NORTH);
			content.add(cancelPanel, BorderLayout.SOUTH);
			frame.setContentPane(content);

			saveButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {

					customer.setFirstName(firstNameTextField.getText());
					customer.setSurname(surnameTextField.getText());
					customer.setPPS(pPSTextField.getText());
					customer.setDOB(dOBTextField.getText());
					customer.setId(customerIDTextField.getText());
					customer.setPassword(passwordTextField.getText());

					JOptionPane.showMessageDialog(null, "Changes Saved.");
				}
			});

			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					frame.dispose();
					menu.adminstrationMenu();
				}
			});
		}
	}

}
