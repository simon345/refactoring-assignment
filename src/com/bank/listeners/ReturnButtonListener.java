package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.bank.main.Menu;

public class ReturnButtonListener implements ActionListener {
	
	private Menu menu;
	
	public ReturnButtonListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		menu.getFrame().dispose();
		menu.mainMenu();
	}

}
