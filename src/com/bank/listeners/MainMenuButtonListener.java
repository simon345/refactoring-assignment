package com.bank.listeners;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.bank.entities.Bank;
import com.bank.entities.Customer;
import com.bank.jframes.DefaultJFrame;
import com.bank.jpanels.CustomerDetailsPanel;
import com.bank.main.Menu;

public class MainMenuButtonListener implements ActionListener {

	private Menu menu;
	private Bank bank;
	private ButtonGroup userType;
	private JFrame frame;
	private Customer customer;

	private static final String ADMIN_USERNAME = "admin";
	private static final String ADMIN_PASSWORD = "admin11";

	public MainMenuButtonListener(Menu driver, ButtonGroup userType, JFrame frame, Bank bank) {
		this.menu = driver;
		this.userType = userType;
		this.bank = bank;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if(userType.getSelection() != null) {
			String radioButtonChoice = userType.getSelection().getActionCommand();

			if (radioButtonChoice.equals("New Customer")) {

				createNewCustomerFrame();
			}

			if (radioButtonChoice.equals("Administrator")) {

				createAdministrationFrame();
			}

			if (radioButtonChoice.equals("Customer")) {

				createCustomerFrame();
			}
		}

	}

	private void createCustomerFrame() {

		frame.dispose();
		frame = new DefaultJFrame("Customer");
		frame.setVisible(true);
		menu.setFrame(frame);

		boolean customerIdIsCorrect = false;
		boolean customerPasswordLoop = true;
		boolean cont = false;
		boolean found = false;
		
		while (!customerIdIsCorrect) {

			String customerID = JOptionPane.showInputDialog(frame, "Enter Customer ID:");
			ArrayList<Customer> customers = bank.getCustomers();

			for (Customer c : customers) {

				if (c.getId().equalsIgnoreCase(customerID)) {
					found = true;
					System.out.println("CUSTOMER FOUND!");
					customer = c;
					break;
				}
			}

			if (found == false) {

				int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);

				if (reply == JOptionPane.YES_OPTION) {
					customerIdIsCorrect = true;

				} else if (reply == JOptionPane.NO_OPTION) {
					frame.dispose();
					customerIdIsCorrect = true;
					customerPasswordLoop = false;
					menu.mainMenu();
				}

			} else {
				customerIdIsCorrect = true;
			}

		}

		while (customerPasswordLoop) {

			String customerPassword = JOptionPane.showInputDialog(frame, "Enter Customer Password;");

			if (customer == null) {
				System.out.println("CUSTOMER IS NULL");
			}

			if (!customer.getPassword().equals(customerPassword)) {

				int reply = JOptionPane.showConfirmDialog(null, null, "Incorrect password. Try again?", JOptionPane.YES_NO_OPTION);

				if (reply == JOptionPane.NO_OPTION) {
					frame.dispose();
					customerPasswordLoop = false;
					menu.mainMenu();
				}

			} else {
				customerPasswordLoop = false;
				cont = true;
			}
		}

		if (cont) {
			frame.dispose();
			customerIdIsCorrect = false;
			menu.existingCustomerMenu(customer);
		}
	}

	private void createAdministrationFrame() {
		frame.dispose();
		frame = new DefaultJFrame("Administrator");
		frame.setVisible(true);
		menu.setFrame(frame);

		boolean adminUsernameEnteredCorrectly = false;
		boolean adminPasswordEnteredCorrectly = false;
		boolean continueToAdminPage = false;
		
		while (!adminUsernameEnteredCorrectly) {

			String adminUsername = JOptionPane.showInputDialog(frame, "Enter Administrator Username:");

			if (!adminUsername.equalsIgnoreCase(ADMIN_USERNAME)) {

				int reply = JOptionPane.showConfirmDialog(null, null, "Incorrect Username. Try again?", JOptionPane.YES_NO_OPTION);

				if (reply == JOptionPane.YES_OPTION) {

					adminUsernameEnteredCorrectly = false;

				} else if (reply == JOptionPane.NO_OPTION) {
					frame.dispose();
					adminUsernameEnteredCorrectly = true;
					adminPasswordEnteredCorrectly = true;
					menu.mainMenu();
				}
				
			} else {
				adminUsernameEnteredCorrectly = true;
			}
		}

		while (!adminPasswordEnteredCorrectly) {

			String adminPassword = JOptionPane.showInputDialog(frame, "Enter Administrator Password;");

			if (!adminPassword.equals(ADMIN_PASSWORD)) {
				int reply = JOptionPane.showConfirmDialog(null, null, "Incorrect Password. Try again?", JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {

				} else if (reply == JOptionPane.NO_OPTION) {
					frame.dispose();
					adminPasswordEnteredCorrectly = true;
					menu.mainMenu();
				}
			} else {
				adminPasswordEnteredCorrectly = true;
				continueToAdminPage = true;
			}

		}

		if (continueToAdminPage) {
			frame.dispose();
			menu.adminstrationMenu();
		}
	}

	private void createNewCustomerFrame() {
		frame.dispose();
		frame = new DefaultJFrame("New Customer");
		frame.setVisible(true);
		menu.setFrame(frame);

		Container content = frame.getContentPane();
		content.setLayout(new BorderLayout());
		
		CustomerDetailsPanel customerDetailsPanel = new CustomerDetailsPanel();

		JPanel buttonsPanel = new JPanel();

		JButton addButton = new JButton("Add");

		AddButtonListener addButtonListener = new AddButtonListener(menu, customerDetailsPanel);
		addButton.addActionListener(addButtonListener);

		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menu.mainMenu();
			}
		});

		buttonsPanel.add(addButton);
		buttonsPanel.add(cancel);

		content.add(customerDetailsPanel, BorderLayout.CENTER);
		content.add(buttonsPanel, BorderLayout.SOUTH);

		frame.setVisible(true);
	}

}
