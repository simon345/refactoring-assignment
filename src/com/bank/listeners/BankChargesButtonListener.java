package com.bank.listeners;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.bank.entities.Customer;
import com.bank.entities.CustomerAccount;
import com.bank.jframes.DefaultJFrame;
import com.bank.main.Menu;
import com.bank.operations.TransactionOperations;

public class BankChargesButtonListener implements ActionListener {

	private Menu menu;
	private JFrame frame;
	private CustomerAccount acc;

	public BankChargesButtonListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		boolean customerIdEnteredCorrectly = false;
		boolean customerFound = false;
		frame = menu.getFrame();
		List<Customer> customerList = menu.getBank().getCustomers();
		Customer customer = menu.getBank().getCustomer();
		acc = menu.getBank().getAccount();

		if (customerList.isEmpty()) {
			JOptionPane.showMessageDialog(frame, "There are no customers yet!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			frame.dispose();
			menu.adminstrationMenu();

		} else {

			while (!customerIdEnteredCorrectly) {

				String customerID = JOptionPane.showInputDialog(frame, "Customer ID of Customer You Wish to Apply Charges to:");

				for (Customer aCustomer : customerList) {

					if (aCustomer.getId().equals(customerID)) {
						customerFound = true;
						customer = aCustomer;
						customerIdEnteredCorrectly = true;
					}
				}

				if (customerFound == false) {

					int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION) {
						customerIdEnteredCorrectly = false;
					} else if (reply == JOptionPane.NO_OPTION) {
						frame.dispose();
						customerIdEnteredCorrectly = true;
						menu.adminstrationMenu();
					}
				} else {

					applyBankCharge(customer);

				}
			}
		}
	}

	private void applyBankCharge(Customer customer) {

		frame.dispose();
		frame = new DefaultJFrame("Apply Bank Charge");
		frame.setVisible(true);

		JComboBox<String> box = new JComboBox<String>();
		for (int i = 0; i < customer.getAccounts().size(); i++) {

			box.addItem(customer.getAccounts().get(i).getNumber());
		}

		box.getSelectedItem();

		JPanel boxPanel = new JPanel();
		boxPanel.add(box);

		JPanel buttonPanel = new JPanel();
		JButton applyChargeButton = new JButton("Apply Charge");
		JButton returnButton = new JButton("Return");
		buttonPanel.add(applyChargeButton);
		buttonPanel.add(returnButton);
		Container content = frame.getContentPane();
		content.setLayout(new GridLayout(2, 1));
		content.add(boxPanel);
		content.add(buttonPanel);

		if (customer.hasNoAccounts()) {
			JOptionPane.showMessageDialog(frame, "This customer has no accounts! \n The admin must add acounts to this customer.", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			frame.dispose();
			menu.adminstrationMenu();
		} else {

			String selectedAccount = (String) box.getSelectedItem();
			acc = customer.getAccountByNumber(selectedAccount);

			applyChargeButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {

					TransactionOperations operations = new TransactionOperations(acc, frame);
					acc = operations.applyBankCharge();

					frame.dispose();
					menu.adminstrationMenu();
				}
			});

			returnButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					frame.dispose();
					menu.mainMenu();
				}
			});

		}
	}
}
