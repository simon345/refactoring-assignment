package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.bank.entities.CustomerAccount;
import com.bank.enums.TransactionType;
import com.bank.main.Menu;
import com.bank.operations.TransactionOperations;

public class LodgementButtonListener implements ActionListener {

	private Menu menu;

	public LodgementButtonListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		CustomerAccount acc = menu.getBank().getAccount();
		TransactionOperations operation = new TransactionOperations(TransactionType.LODGEMENT, acc, menu);
		operation.performTransaction();
	}
}
