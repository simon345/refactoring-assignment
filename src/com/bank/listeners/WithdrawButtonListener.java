package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.bank.entities.Bank;
import com.bank.entities.CustomerAccount;
import com.bank.enums.TransactionType;
import com.bank.main.Menu;
import com.bank.operations.TransactionOperations;

public class WithdrawButtonListener implements ActionListener {

	private Menu menu;
	private Bank bank;
	
	public WithdrawButtonListener(Menu menu) {
		this.menu = menu;
		this.bank = menu.getBank();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		CustomerAccount acc = bank.getAccount();
		TransactionOperations operation = new TransactionOperations(TransactionType.WITHDRAWAL, acc, menu);
		operation.performTransaction();
		
	}
}
