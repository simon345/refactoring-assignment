package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.bank.entities.Customer;
import com.bank.main.Menu;

public class DeleteCustomerButtonListener implements ActionListener {

	private Menu menu;
	private JFrame frame;

	public DeleteCustomerButtonListener(Menu menu) {
		this.menu = menu;
	}

	@SuppressWarnings("unused")
	@Override
	public void actionPerformed(ActionEvent e) {

		frame = menu.getFrame();
		List<Customer> customerList = menu.getBank().getCustomers();
		Customer customer = menu.getBank().getCustomer();
		boolean found = true;
		boolean loop = true;

		if (customerList.isEmpty()) {
			JOptionPane.showMessageDialog(null, "There are currently no customers to display. ");
			menu.dispose();
			menu.adminstrationMenu();
		} else {
			{
				String customerID = JOptionPane.showInputDialog(frame, "Customer ID of Customer You Wish to Delete:");

				for (Customer aCustomer : customerList) {

					if (aCustomer.getId().equals(customerID)) {
						found = true;
						customer = aCustomer;
						loop = false;
					}
				}

				if (found == false) {
					int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION) {
						loop = true;
					} else if (reply == JOptionPane.NO_OPTION) {
						frame.dispose();
						loop = false;

						menu.adminstrationMenu();
					}
				} else {
					if (customer.getAccounts().size() > 0) {
						JOptionPane.showMessageDialog(frame, "This customer has accounts. \n You must delete a customer's accounts before deleting a customer ", "Oops!", JOptionPane.INFORMATION_MESSAGE);
					} else {
						customerList.remove(customer);
						JOptionPane.showMessageDialog(frame, "Customer Deleted ", "Success.", JOptionPane.INFORMATION_MESSAGE);
					}
				}

			}
		}
	}

}
