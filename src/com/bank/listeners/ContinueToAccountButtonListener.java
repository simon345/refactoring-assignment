package com.bank.listeners;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import com.bank.entities.Customer;
import com.bank.entities.CustomerAccount;
import com.bank.jframes.DefaultJFrame;
import com.bank.jpanels.ExistingCustomerMenuPanel;
import com.bank.main.Menu;

public class ContinueToAccountButtonListener implements ActionListener {

	private Menu menu;
	private JComboBox<String> box;

	public ContinueToAccountButtonListener(Menu menu, JComboBox<String> box) {

		this.menu = menu;
		this.box = box;

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		Customer currentCustomer = menu.getBank().getCustomer();
		CustomerAccount account = menu.getBank().getAccount();
		JFrame frame = menu.getFrame();

		for (CustomerAccount acc : currentCustomer.getAccounts()) {
			if (acc.getNumber() == box.getSelectedItem()) {
				account = acc;
				menu.getBank().setAccount(account);
				break;
			}
		}

		frame.dispose();
		frame = new DefaultJFrame("Customer Menu");
		frame.setVisible(true);

		ExistingCustomerMenuPanel existingCustomerMenuPanel = new ExistingCustomerMenuPanel(menu);

		Container content = menu.getContent();
		content = frame.getContentPane();
		content.add(existingCustomerMenuPanel);

	}

}
