package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.bank.entities.Customer;
import com.bank.entities.CustomerAccount;
import com.bank.jpanels.CustomerDetailsPanel;
import com.bank.main.Menu;

public class AddButtonListener implements ActionListener {

	private Menu menu;
	private JFrame frame;
	private CustomerDetailsPanel customerDetailsPanel;

	public AddButtonListener(Menu menu, CustomerDetailsPanel customerDetailsPanel) {
		this.menu = menu;
		this.customerDetailsPanel = customerDetailsPanel;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		frame = menu.getFrame();

		String PPS = customerDetailsPanel.getPPSTextFieldText();
		String firstName = customerDetailsPanel.getFirstNameTextFieldText();
		String surname = customerDetailsPanel.getSurnameTextFieldText();
		String DOB = customerDetailsPanel.getdOBTextFieldText();
		String password = customerDetailsPanel.getPasswordTextFieldText();
		String CustomerID = "ID" + PPS;

		if (password.length() != 7) {
			JOptionPane.showMessageDialog(null, null, "Password must be 7 characters long", JOptionPane.OK_OPTION);

		} else {
			createCustomer(PPS, firstName, surname, DOB, password, CustomerID);
		}

	}

	private void createCustomer(String PPS, String firstName, String surname, String DOB, String password, String CustomerID) {

		ArrayList<CustomerAccount> accounts = new ArrayList<CustomerAccount>();
		Customer customer = new Customer(PPS, surname, firstName, DOB, CustomerID, password, accounts);
		menu.getBank().addCustomer(customer);
		JOptionPane.showMessageDialog(frame, "CustomerID = " + CustomerID + "\n Password = " + password, "Customer created.", JOptionPane.INFORMATION_MESSAGE);
		frame.dispose();
		menu.mainMenu();
	}

}
