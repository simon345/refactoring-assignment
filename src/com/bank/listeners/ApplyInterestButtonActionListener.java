package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.bank.entities.CustomerAccount;
import com.bank.main.Menu;
import com.bank.operations.TransactionOperations;

public class ApplyInterestButtonActionListener implements ActionListener {

	private Menu menu;

	public ApplyInterestButtonActionListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		JFrame frame = menu.getFrame();
		CustomerAccount account = menu.getBank().getAccount();

		TransactionOperations transactionStrategy = new TransactionOperations(account, frame, menu);
		account = transactionStrategy.addInterestToAccount();

		frame.dispose();
	}

}
