package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.bank.entities.Customer;
import com.bank.factory.BankAccountFactory;
import com.bank.main.Menu;

public class AccountButtonListener implements ActionListener {

	private Menu menu;
	private JFrame frame;

	public AccountButtonListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		Customer customer = menu.getBank().getCustomer();
		frame = menu.getFrame();
		frame.dispose();

		if (menu.getBank().getCustomers() == null) {
			JOptionPane.showMessageDialog(frame, "There are no customers yet!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
			frame.dispose();
			menu.adminstrationMenu();
		} else {

			List<Customer> customerList = menu.getBank().getCustomers();

			boolean customerIdCorrectlyEntered = false;
			boolean userExists = false;

			while (!customerIdCorrectlyEntered) {

				String customerID = JOptionPane.showInputDialog(frame, "Customer ID of Customer You Wish to Add an Account to:");

				for (Customer c : customerList) {

					if (c.getId().equals(customerID)) {
						userExists = true;
						customer = c;
					}
				}

				if (userExists == false) {
					int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION) {
						customerIdCorrectlyEntered = false;
					} else if (reply == JOptionPane.NO_OPTION) {
						frame.dispose();
						customerIdCorrectlyEntered = true;
						menu.adminstrationMenu();
					}
				} else {

					customerIdCorrectlyEntered = true;
					customer = createAccount(customer, customerList);
				}
			}

		}
	}

	private Customer createAccount(Customer customer, List<Customer> customerList) {

		String[] choices = { "Current Account", "Deposit Account" };
		String accountTypeChoice = (String) JOptionPane.showInputDialog(null, "Please choose account type", "Account Type", JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);

		BankAccountFactory accountFactory = BankAccountFactory.getInstance();
		customer = accountFactory.addAccountToCustomer(accountTypeChoice, customer, customerList);
		
		frame.dispose();
		menu.adminstrationMenu();
		return customer;
	}

}
