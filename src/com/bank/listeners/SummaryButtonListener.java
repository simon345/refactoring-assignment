package com.bank.listeners;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.bank.entities.AccountTransaction;
import com.bank.entities.Customer;
import com.bank.entities.CustomerAccount;
import com.bank.jframes.DefaultJFrame;
import com.bank.main.Menu;

/**
 * 
 * @author Simon
 *
 */
public class SummaryButtonListener implements ActionListener {

	private Menu menu;
	private JFrame frame;

	public SummaryButtonListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		List<Customer> customerList = menu.getBank().getCustomers();
		frame = menu.getFrame();

		frame.dispose();

		frame = new DefaultJFrame("Summary");
		frame.setVisible(true);

		JLabel heading = new JLabel("Summary of all transactions: ");

		JPanel returnPanel = new JPanel();
		JButton returnButton = new JButton("Return");
		returnPanel.add(returnButton);

		JPanel textPanel = new JPanel();

		textPanel.setLayout(new BorderLayout());
		JTextArea textArea = new JTextArea(40, 20);
		textArea.setEditable(false);
		textPanel.add(heading, BorderLayout.NORTH);
		textPanel.add(textArea, BorderLayout.CENTER);
		textPanel.add(returnButton, BorderLayout.SOUTH);

		JScrollPane scrollPane = new JScrollPane(textArea);
		textPanel.add(scrollPane);

		for (Customer customer : customerList) {
			textArea.append("Customer " + customer.getName() + "\n");
			for (CustomerAccount account : customer.getAccounts()) {
				textArea.append(account.getNumber() + "\n");
				for (AccountTransaction transaction : account.getTransactionList()) {
					textArea.append(transaction.toString() + "\n");
				}
			}
			textArea.append("\n");
		}

		textPanel.add(textArea);
		Container content = menu.getContent();
		content.removeAll();

		content = frame.getContentPane();
		content.setLayout(new GridLayout(1, 1));
		content.add(textPanel);

		returnButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				frame.dispose();
				menu.adminstrationMenu();
			}
		});
	}

}
