package com.bank.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.bank.entities.Customer;
import com.bank.main.Menu;

public class DeleteAccountButtonListener implements ActionListener {

	private Menu menu;

	public DeleteAccountButtonListener(Menu menu) {
		this.menu = menu;
	}

	@SuppressWarnings("unused")
	@Override
	public void actionPerformed(ActionEvent e) {

		JFrame frame = menu.getFrame();
		List<Customer> customerList = menu.getBank().getCustomers();
		Customer customer = menu.getBank().getCustomer();
		boolean found = true;
		boolean loop = true;

		{
			String customerID = JOptionPane.showInputDialog(frame, "Customer ID of Customer from which you wish to delete an account");

			for (Customer aCustomer : customerList) {

				if (aCustomer.getId().equals(customerID)) {
					found = true;
					customer = aCustomer;
					loop = false;
				}
			}

			if (found == false) {
				int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					loop = true;
				} else if (reply == JOptionPane.NO_OPTION) {
					frame.dispose();
					loop = false;
					menu.adminstrationMenu();
				}
			} else {
				// Here I would make the user select a an account to
				// delete from a combo box. If the account had a balance
				// of 0 then it would be deleted. (I do not have time to
				// do this)
			}

		}
	}

}
