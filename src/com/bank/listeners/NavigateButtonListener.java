package com.bank.listeners;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.bank.entities.Customer;
import com.bank.main.Menu;

public class NavigateButtonListener implements ActionListener {

	private Menu menu;
	private int position;

	private JLabel firstNameLabel;;
	private JLabel surnameLabel;
	private JLabel pPPSLabel;
	private JLabel dOBLabel;
	private JLabel customerIDLabel;
	private JLabel passwordLabel;
	private JTextField firstNameTextField;
	private JTextField surnameTextField;
	private JTextField pPSTextField;
	private JTextField dOBTextField;
	private JTextField customerIDTextField;
	private JTextField passwordTextField;

	public NavigateButtonListener(Menu menu) {

		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		List<Customer> customerList = menu.getBank().getCustomers();
		menu.dispose();

		if (customerList.isEmpty()) {

			JOptionPane.showMessageDialog(null, "There are currently no customers to display. ");
			menu.adminstrationMenu();

		} else {

			Container content = menu.getContentPane();
			content.setLayout(new BorderLayout());

			JPanel buttonPanel = new JPanel();
			JPanel gridPanel = new JPanel(new GridLayout(8, 2));
			JPanel cancelPanel = new JPanel();

			firstNameLabel = new JLabel("First Name:", SwingConstants.LEFT);
			surnameLabel = new JLabel("Surname:", SwingConstants.LEFT);
			pPPSLabel = new JLabel("PPS Number:", SwingConstants.LEFT);
			dOBLabel = new JLabel("Date of birth", SwingConstants.LEFT);
			customerIDLabel = new JLabel("CustomerID:", SwingConstants.LEFT);
			passwordLabel = new JLabel("Password:", SwingConstants.LEFT);

			firstNameTextField = new JTextField(20);
			firstNameTextField.setEditable(false);

			surnameTextField = new JTextField(20);
			surnameTextField.setEditable(false);

			pPSTextField = new JTextField(20);
			pPSTextField.setEditable(false);

			dOBTextField = new JTextField(20);
			dOBTextField.setEditable(false);

			customerIDTextField = new JTextField(20);
			customerIDTextField.setEditable(false);

			passwordTextField = new JTextField(20);
			passwordTextField.setEditable(false);

			JButton first = new JButton("First");
			JButton previous = new JButton("Previous");
			JButton next = new JButton("Next");
			JButton last = new JButton("Last");
			JButton cancel = new JButton("Cancel");

			Customer firstCustomer = customerList.get(0);
			displayCustomer(firstCustomer);

			gridPanel.add(firstNameLabel);
			gridPanel.add(firstNameTextField);
			gridPanel.add(surnameLabel);
			gridPanel.add(surnameTextField);
			gridPanel.add(pPPSLabel);
			gridPanel.add(pPSTextField);
			gridPanel.add(dOBLabel);
			gridPanel.add(dOBTextField);
			gridPanel.add(customerIDLabel);
			gridPanel.add(customerIDTextField);
			gridPanel.add(passwordLabel);
			gridPanel.add(passwordTextField);

			buttonPanel.add(first);
			buttonPanel.add(previous);
			buttonPanel.add(next);
			buttonPanel.add(last);

			cancelPanel.add(cancel);

			content.add(gridPanel, BorderLayout.NORTH);
			content.add(buttonPanel, BorderLayout.CENTER);
			content.add(cancelPanel, BorderLayout.AFTER_LAST_LINE);

			first.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					Customer firstCustomer = customerList.get(0);
					displayCustomer(firstCustomer);
				}
			});

			previous.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {

					if (position >= 1) {
						position = position - 1;
						Customer currentCustomer = customerList.get(position);
						displayCustomer(currentCustomer);

					}
				}
			});

			next.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {

					int numberOfCustomers = customerList.size() - 1;

					if (position != numberOfCustomers) {
						position = position + 1;
						Customer nextCustomer = customerList.get(position);
						displayCustomer(nextCustomer);

					}

				}
			});

			last.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {

					int lastCustomerPosition = customerList.size() - 1;
					Customer lastCustomer = customerList.get(lastCustomerPosition);
					displayCustomer(lastCustomer);

				}
			});

			cancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					menu.dispose();
					menu.adminstrationMenu();
				}
			});

			menu.setContentPane(content);
			menu.setSize(400, 300);
			menu.setVisible(true);

		}
	}

	private void displayCustomer(Customer customer) {

		firstNameTextField.setText(customer.getFirstName());
		surnameTextField.setText(customer.getSurname());
		pPSTextField.setText(customer.getPPS());
		dOBTextField.setText(customer.getDOB());
		customerIDTextField.setText(customer.getId());
		passwordTextField.setText(customer.getPassword());
	}
}
