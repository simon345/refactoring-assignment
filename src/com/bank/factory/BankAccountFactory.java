package com.bank.factory;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.bank.entities.ATMCard;
import com.bank.entities.Customer;
import com.bank.entities.CustomerCurrentAccount;
import com.bank.entities.CustomerDepositAccount;

public class BankAccountFactory {

	private static BankAccountFactory factory = new BankAccountFactory();

	private double startingBalance = 0;
	private Customer customer;
	private List<Customer> customerList;
	private JFrame currentFrame;

	private BankAccountFactory() {
	}

	public static BankAccountFactory getInstance() {
		return factory;
	}

	public Customer addAccountToCustomer(String accountTypeChoice, Customer customer, List<Customer> customerList) {

		this.customerList = customerList;
		this.customer = customer;
		System.out.println(customer.getName() + "in add account to customer");

		if (accountTypeChoice.equalsIgnoreCase("Current Account")) {

			customer.addAccount(createNewCurrentAccount());
			int newAccountIndex = customer.getAccounts().size() - 1;
			CustomerCurrentAccount account = (CustomerCurrentAccount) customer.getAccounts().get(newAccountIndex);
			JOptionPane.showMessageDialog(currentFrame, "Account number = " + account.getNumber() + "\n PIN = " + account.getAtm().getPin(), "Account created.", JOptionPane.INFORMATION_MESSAGE);

		} else if (accountTypeChoice.equalsIgnoreCase("Deposit Account")) {

			customer.addAccount(createNewDepositAccount());
			int newAccountIndex = customer.getAccounts().size() - 1;
			JOptionPane.showMessageDialog(currentFrame, "Account number = " + customer.getAccounts().get(newAccountIndex).getNumber(), "Account created.", JOptionPane.INFORMATION_MESSAGE);

		}

		System.out.println(customer.getName());

		return customer;
	}

	private CustomerDepositAccount createNewDepositAccount() {

		CustomerDepositAccount account = new CustomerDepositAccount();

		account.setBalance(startingBalance);

		String accountNumber = accountNumberGenerator("D");
		account.setNumber(accountNumber);

		account.setInterestRate(0.0);

		return account;
	}

	private CustomerCurrentAccount createNewCurrentAccount() {

		CustomerCurrentAccount account = new CustomerCurrentAccount();

		account.setBalance(startingBalance);

		String accountNumber = accountNumberGenerator("C");
		account.setNumber(accountNumber);

		int atmCardPin = generateRandomPin();
		ATMCard atmCard = new ATMCard(atmCardPin, true);

		account.setAtm(atmCard);

		return account;

	}

	private int generateRandomPin() {
		return (int) (Math.random() * 9000) + 1000;

	}

	private String accountNumberGenerator(String prefix) {
		return String.valueOf(prefix + (customerList.indexOf(customer) + 1) * 10 + (customer.getAccounts().size() + 1));
	}

}
