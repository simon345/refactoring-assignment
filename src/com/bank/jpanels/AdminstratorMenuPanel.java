package com.bank.jpanels;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bank.listeners.AccountButtonListener;
import com.bank.listeners.BankChargesButtonListener;
import com.bank.listeners.DeleteAccountButtonListener;
import com.bank.listeners.DeleteCustomerButtonListener;
import com.bank.listeners.EditCustomerButtonListener;
import com.bank.listeners.InterestButtonListener;
import com.bank.listeners.NavigateButtonListener;
import com.bank.listeners.SummaryButtonListener;
import com.bank.main.Menu;

public class AdminstratorMenuPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6953975870436088367L;
	private Dimension dimension = new Dimension(250, 20);
	private FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
	
	public AdminstratorMenuPanel(Menu driver) {
		
		this.setLayout(new GridLayout(8, 1));

		JPanel deleteCustomerPanel = new JPanel(flow);
		JButton deleteCustomer = new JButton("Delete Customer");
		deleteCustomer.setPreferredSize(dimension);
		deleteCustomerPanel.add(deleteCustomer);

		JPanel deleteAccountPanel = new JPanel(flow);
		JButton deleteAccount = new JButton("Delete Account");
		deleteAccount.setPreferredSize(dimension);
		deleteAccountPanel.add(deleteAccount);

		JPanel bankChargesPanel = new JPanel(flow);
		JButton bankChargesButton = new JButton("Apply Bank Charges");
		bankChargesButton.setPreferredSize(dimension);
		bankChargesPanel.add(bankChargesButton);

		JPanel interestPanel = new JPanel(flow);
		JButton interestButton = new JButton("Apply Interest");
		interestPanel.add(interestButton);
		interestButton.setPreferredSize(dimension);

		JPanel editCustomerPanel = new JPanel(flow);
		JButton editCustomerButton = new JButton("Edit existing Customer");
		editCustomerPanel.add(editCustomerButton);
		editCustomerButton.setPreferredSize(dimension);

		JPanel navigatePanel = new JPanel(flow);
		JButton navigateButton = new JButton("Navigate Customer Collection");
		navigatePanel.add(navigateButton);
		navigateButton.setPreferredSize(dimension);

		JPanel summaryPanel = new JPanel(flow);
		JButton summaryButton = new JButton("Display Summary Of All Accounts");
		summaryPanel.add(summaryButton);
		summaryButton.setPreferredSize(dimension);

		JPanel accountPanel = new JPanel(flow);
		JButton accountButton = new JButton("Add an Account to a Customer");
		accountPanel.add(accountButton);
		accountButton.setPreferredSize(dimension);
		
		JLabel label1 = new JLabel("Please select an option");	
	
		BankChargesButtonListener bankChargesButtonListener = new BankChargesButtonListener(driver);
		bankChargesButton.addActionListener(bankChargesButtonListener);

		InterestButtonListener interestButtonListener = new InterestButtonListener(driver);
		interestButton.addActionListener(interestButtonListener);

		EditCustomerButtonListener editCustomerButtonListener = new EditCustomerButtonListener(driver);
		editCustomerButton.addActionListener(editCustomerButtonListener);

		SummaryButtonListener summaryButtonListener = new SummaryButtonListener(driver);
		summaryButton.addActionListener(summaryButtonListener);

		NavigateButtonListener navigateButtonListener = new NavigateButtonListener(driver);
		navigateButton.addActionListener(navigateButtonListener);

		AccountButtonListener accountButtonListener = new AccountButtonListener(driver);
		accountButton.addActionListener(accountButtonListener);

		DeleteCustomerButtonListener deleteCustomerButtonListener = new DeleteCustomerButtonListener(driver);
		deleteCustomer.addActionListener(deleteCustomerButtonListener);

		DeleteAccountButtonListener deleteAccountButtonListener = new DeleteAccountButtonListener(driver);
		deleteAccount.addActionListener(deleteAccountButtonListener);

		this.add(label1);
		this.add(accountPanel);
		this.add(bankChargesPanel);
		this.add(interestPanel);
		this.add(editCustomerPanel);
		this.add(navigatePanel);
		this.add(summaryPanel);
		this.add(deleteCustomerPanel);
		//this.add(deleteAccountPanel);
		
	}

}
