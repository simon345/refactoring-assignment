package com.bank.jpanels;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bank.entities.Customer;
import com.bank.listeners.ContinueToAccountButtonListener;
import com.bank.listeners.ReturnButtonListener;
import com.bank.main.Menu;

public class ExistingCustomerAccountSelectionPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4302550274650535637L;

	@SuppressWarnings("unused")
	private Menu menu;

	public ExistingCustomerAccountSelectionPanel(Menu menu) {

		this.setLayout(new GridLayout(3, 1));

		Customer currentCustomer = menu.getBank().getCustomer();

		JPanel buttonPanel = new JPanel();
		JPanel boxPanel = new JPanel();
		JPanel labelPanel = new JPanel();

		JLabel label = new JLabel("Select Account:");
		labelPanel.add(label);

		JButton returnButton = new JButton("Return");
		buttonPanel.add(returnButton);
		JButton continueToAccountsButton = new JButton("Continue");
		buttonPanel.add(continueToAccountsButton);

		JComboBox<String> accountsListComboBox = new JComboBox<String>();
		for (int i = 0; i < currentCustomer.getAccounts().size(); i++) {
			accountsListComboBox.addItem(currentCustomer.getAccounts().get(i).getNumber());
		}

		boxPanel.add(accountsListComboBox);

		ReturnButtonListener returnButtonListener = new ReturnButtonListener(menu);
		returnButton.addActionListener(returnButtonListener);

		ContinueToAccountButtonListener continueToAccountButtonListener = new ContinueToAccountButtonListener(menu, accountsListComboBox);
		continueToAccountsButton.addActionListener(continueToAccountButtonListener);

		this.add(labelPanel);
		this.add(boxPanel);
		this.add(buttonPanel);

	}

}
