package com.bank.jpanels;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class MainMenuPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 292909315783935584L;
	private ButtonGroup userType;
	
	public MainMenuPanel() {
		this.userType = new ButtonGroup();
		JRadioButton radioButton;
		this.add(radioButton = new JRadioButton("Existing Customer"));
		radioButton.setActionCommand("Customer");
		userType.add(radioButton);

		this.add(radioButton = new JRadioButton("Administrator"));
		radioButton.setActionCommand("Administrator");
		userType.add(radioButton);

		this.add(radioButton = new JRadioButton("New Customer"));
		radioButton.setActionCommand("New Customer");
		userType.add(radioButton);

	}

	public ButtonGroup getUserType() {
		return userType;
	}
}
