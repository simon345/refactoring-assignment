package com.bank.jpanels;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bank.listeners.LodgementButtonListener;
import com.bank.listeners.ReturnButtonListener;
import com.bank.listeners.StatementButtonListener;
import com.bank.listeners.WithdrawButtonListener;
import com.bank.main.Menu;

public class ExistingCustomerMenuPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4312143632827140624L;

	@SuppressWarnings("unused")
	private Menu menu;

	public ExistingCustomerMenuPanel(Menu menu) {

		this.setLayout(new GridLayout(5, 1));

		this.menu = menu;

		Dimension dimension = new Dimension(250, 20);
		FlowLayout flow = new FlowLayout(FlowLayout.LEFT);

		JPanel statementPanel = new JPanel(flow);
		JButton statementButton = new JButton("Display Bank Statement");
		statementButton.setPreferredSize(dimension);
		statementPanel.add(statementButton);

		JPanel lodgementPanel = new JPanel(flow);
		JButton lodgementButton = new JButton("Lodge money into account");
		lodgementPanel.add(lodgementButton);
		lodgementButton.setPreferredSize(dimension);

		JPanel withdrawalPanel = new JPanel(flow);
		JButton withdrawButton = new JButton("Withdraw money from account");
		withdrawalPanel.add(withdrawButton);
		withdrawButton.setPreferredSize(dimension);

		JPanel returnPanel = new JPanel(flow);
		JButton returnButton = new JButton("Exit Customer Menu");
		returnPanel.add(returnButton);

		JLabel heading = new JLabel("Please select an option");

		StatementButtonListener statementButtonListener = new StatementButtonListener(menu);
		statementButton.addActionListener(statementButtonListener);

		LodgementButtonListener lodgementButtonListener = new LodgementButtonListener(menu);
		lodgementButton.addActionListener(lodgementButtonListener);

		WithdrawButtonListener withdrawButtonListener = new WithdrawButtonListener(menu);
		withdrawButton.addActionListener(withdrawButtonListener);

		ReturnButtonListener returnButtonListener = new ReturnButtonListener(menu);
		returnButton.addActionListener(returnButtonListener);

		this.add(heading);
		this.add(statementPanel);
		this.add(lodgementPanel);
		this.add(withdrawalPanel);
		this.add(returnPanel);
	}

}
