package com.bank.jpanels;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class CustomerDetailsPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4984436595560795002L;
	private JLabel firstNameLabel;
	private JLabel surnameLabel;
	private JLabel ppsLabel;
	private JLabel dobLabel;
	private JLabel passwordLabel;

	private JTextField firstNameTextField;
	private JTextField surnameTextField;
	private JTextField pPSTextField;
	private JTextField dOBTextField;
	private JPasswordField passwordTextField;

	public CustomerDetailsPanel() {

		firstNameLabel = new JLabel("First Name:", SwingConstants.RIGHT);
		surnameLabel = new JLabel("Surname:", SwingConstants.RIGHT);
		ppsLabel = new JLabel("PPS Number:", SwingConstants.RIGHT);
		dobLabel = new JLabel("Date of birth", SwingConstants.RIGHT);
		passwordLabel = new JLabel("Password", SwingConstants.RIGHT);
		
		firstNameTextField = new JTextField(20);
		surnameTextField = new JTextField(20);
		pPSTextField = new JTextField(20);
		dOBTextField = new JTextField(20);
		passwordTextField = new JPasswordField(7);

		this.setLayout(new GridLayout(6, 2));

		this.add(firstNameLabel);
		this.add(firstNameTextField);
		this.add(surnameLabel);
		this.add(surnameTextField);
		this.add(ppsLabel);
		this.add(pPSTextField);
		this.add(dobLabel);
		this.add(dOBTextField);
		this.add(dobLabel);
		this.add(dOBTextField);
		this.add(passwordLabel);
		this.add(passwordTextField);
	}

	public String getFirstNameTextFieldText() {
		return firstNameTextField.getText();
	}

	public String getSurnameTextFieldText() {
		return surnameTextField.getText();
	}

	public String getPPSTextFieldText() {
		return pPSTextField.getText();
	}

	public String getdOBTextFieldText() {
		return dOBTextField.getText();
	}

	@SuppressWarnings("deprecation")
	public String getPasswordTextFieldText() {
		return passwordTextField.getText();
	}
}
