package com.bank.entities;

import java.util.ArrayList;
import java.util.List;

public class Customer {

	private String pps;
	private String surname;
	private String firstName;
	private String dob;
	private String id;
	private String password;
	private List<CustomerAccount> accounts;

	public Customer() { }

	public Customer(String PPS, String surname, String firstName, String DOB, String customerID, String password, ArrayList<CustomerAccount> accounts) {
		this.pps = PPS;
		this.surname = surname;
		this.firstName = firstName;
		this.dob = DOB;
		this.id = customerID;
		this.password = password;
		;
		this.accounts = accounts;
	}

	public String getName() {
		return this.firstName + " " + this.surname;
	}

	public void addAccount(CustomerAccount account) {
		if (accounts == null) {
			accounts = new ArrayList<CustomerAccount>();
		}

		accounts.add(account);
	}

	public String getPPS() {
		return pps;
	}

	public void setPPS(String PPS) {
		this.pps = PPS;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getDOB() {
		return dob;
	}

	public void setDOB(String dOB) {
		this.dob = dOB;
	}

	public String getId() {
		return id;
	}

	public void setId(String customerID) {
		this.id = customerID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<CustomerAccount> getAccounts() {
		
		if(accounts == null) {
			accounts = new ArrayList<CustomerAccount>();
		}
		
		return accounts;
	}

	public void setAccounts(List<CustomerAccount> accounts) {
		this.accounts = accounts;
	}

	@Override
	public String toString() {
		return "Customer [PPS=" + pps + ", surname=" + surname + ", firstName=" + firstName + ", DOB=" + dob + ", customerID=" + id + ", password=" + password + ", accounts=" + accounts.size() + "]";
	}

	public CustomerAccount getAccountByNumber(String selectedItem) {
		
		CustomerAccount account = null;
		
		for (CustomerAccount acc : accounts) {
			if (acc.getNumber() == selectedItem) {
				account = acc;
				break;
			}
		}
		
		return account;
		
	}

	public boolean hasNoAccounts() {
		
		if(accounts == null) {
			accounts = new ArrayList<CustomerAccount>();
		}
		
		return accounts.isEmpty();
	}

}
