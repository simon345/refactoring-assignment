package com.bank.entities;

import java.util.ArrayList;

public class Bank {

	private ArrayList<Customer> customers;
	private Customer customer;
	private CustomerAccount acc;
	
	public Bank() {	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public CustomerAccount getAccount() {
		return acc;
	}

	public void setAccount(CustomerAccount acc) {
		this.acc = acc;
	}
	
	public void addCustomer(Customer customer) {
		
		if(customers == null) {
			customers = new ArrayList<Customer>();
		}
		
		customers.add(customer);
	}
}
