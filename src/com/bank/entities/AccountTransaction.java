package com.bank.entities;

import java.util.Date;

import com.bank.enums.TransactionType;

public class AccountTransaction {

	private Date date;
	private double amount;
	private TransactionType type;
	
	public AccountTransaction() {}
	
	public AccountTransaction(Date date, TransactionType type, double amount)
	{
		this.date = date;
		this.type = type;
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return date + " " + type.toString() + " : " + amount;
	}	
}
