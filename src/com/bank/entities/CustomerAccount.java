package com.bank.entities;

import java.util.ArrayList;
import java.util.List;

public class CustomerAccount {

	private String number;
	private double balance;
	private List<AccountTransaction> transactions;

	public CustomerAccount() { }

	public CustomerAccount(String number, double balance, ArrayList<AccountTransaction> transactions) {
		this.number = number;
		this.balance = balance;
		this.transactions = transactions;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public double getBalance() {
		
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public List<AccountTransaction> getTransactionList() {
		
		if(transactions == null) {
			transactions = new ArrayList<AccountTransaction>();
		}
		
		return transactions;
	}

	public void setTransactionList(List<AccountTransaction> transactionList) {
		this.transactions = transactionList;
	}

	@Override
	public String toString() {
		return "CustomerAccount [number=" + number + ", balance=" + balance + ", transactionList=" + transactions + "]";
	}

	public void addTransaction(AccountTransaction transaction) {

		if(transactions == null) {
			transactions = new ArrayList<AccountTransaction>();
		}
		
		this.transactions.add(transaction);
	}
	
	
}
