package com.bank.tests;


import org.junit.Assert;
import org.junit.Test;

import com.bank.entities.Bank;
import com.bank.entities.Customer;
import com.bank.entities.CustomerAccount;

public class BankTest {
	
	@Test
	public void addCustomerTest() {
		
		Bank bank = new Bank();
		Customer customer = new Customer();
		customer.setFirstName("TEST");
		
		bank.addCustomer(customer);
		int expectedSize = 1;
		Assert.assertEquals(bank.getCustomers().size(), expectedSize);

		bank.addCustomer(customer);
		expectedSize++;
		Assert.assertEquals(bank.getCustomers().size(), expectedSize);
		
	}
	
	@Test
	public void customerTest() {
		
		Customer customer = new Customer();
		customer.setFirstName("Test");
		customer.setSurname("Test");
		
		String expectedName = customer.getFirstName() + " " + customer.getSurname();
		Assert.assertEquals(customer.getName(), expectedName);
		
		Assert.assertTrue(customer.hasNoAccounts());
		
		CustomerAccount account = new CustomerAccount();
		account.setBalance(100);
		account.setNumber("123");
		customer.addAccount(account);
		
		Assert.assertEquals(customer.getAccounts().size(), 1);
		Assert.assertEquals(customer.getAccountByNumber("123"), account);
		Assert.assertNull(customer.getAccountByNumber("not an account number"));
		
	}

}
